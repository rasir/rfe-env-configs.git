const a = { a: { c: 1, e: 2, f: { g: 1, h: 2 } }, b: ["a"] };
const b = {
  a: { c: 2, f: { g: 3 } },
  b: [1, 2, 3],
  c: undefined,
  d: { f: function () {} },
};
const c = undefined;
const res = mergeObjs(b, c, a);
console.log(res);
