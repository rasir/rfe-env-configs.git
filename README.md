### 前端环境配置(rfe-env-config)

#### 用途

这个工具是为了方便前端为不同环境配置不同的全局变量而准备的。
尽管目前很多打包工具，都能将环境变量注入到全局中，但是如果你在不同环境中有大量需要区分的全局变量，那么这个工具会更方便。

#### 应用场景

比如我们一般开发环境，测试环境，生产环境是分开的。我们有一个跳转链接需要跳转到公司的其他网站（他们也是有环境分区的，假设同公司同环境主域名下 session 是共享的）。为了能顺利跳转到对应的网站的时候能保持登陆状态，我们需要为不同的环境分别配置跳转的目标网址。这样我们就需要在不同的环境中注入全局变量。
再比如，不同环境用到的 logo 不一样，这个工具也可以帮你在打包之前替换掉相关静态文件

```
注意：该工具所有的动作需要在打包之前完成，全局变量的配置文件需要手动引入项目中。
```

#### 安装依赖

`npm i rfe-env-configs`

#### 使用前准备

```
app
    |-- templateDirPath 所有环境对应的模板目录
        ｜-- env 环境名称对应的目录
            ｜-- tempConfigPath 不同环境对应的配置文件，会与根目录下的默认配置文件合并后输出
            ｜-- tempStaticDir 不同环境对应的静态文件目录，该目录下的所有文件都会被拷贝到targetStaticDir
        ｜-- tempDefaultConfigPath 根目录下默认配置文件
    ｜-- targetConfigPath 合并后的配置文件输出的目录，如果没有就不会输出配置文件
    ｜-- targetStaticDir 静态文件最后会被拷贝到这个目录下，在拷贝之前会将templateDirPath 所有 env目录中可能被拷贝到这里的静态文件都清空，所以要注意这个目录下的非环境相关的文件不要和 env 目录下文件重名。

```

#### 命令行

```
rfe -h
rfe/1.0.0

Usage:
  $ rfe <command> [options]

Commands:
  run  复制模版文件，合并配置文件到目标目录

For more info, run any command with the `--help` flag:
  $ rfe run --help

Options:
  -h, --help     Display this message
  -v, --version  Display version number
```

该工具的命令行只有一个 rfe run 具体使用参数参见 rfe -h

```
rfe/1.0.0

Usage:
  $ rfe run

Options:
  --env <env>                                      模板环境
  --root <root>                                    根目录相对地址
  --templateDirPath <templateDirPath>              模板根文件夹路径
  --tempDefaultConfigPath <tempDefaultConfigPath>  模板根文件夹下的公共配置文件路径
  --tempConfigPath <tempConfigPath>                模板文件相对模板文件夹路径
  --tempStaticDir <tempStaticDir>                  模板静态文件夹相对环境目录的路径
  --targetConfigPath <targetConfigPath>            配置文件的目标路径
  --targetStaticDir <targetStaticDir>              静态文件目标目录路径
  -h, --help                                       Display this message
  -v, --version
```

demo
`rfe run --tempStaticDir static --tempConfigPath config.js --templateDirPath temp`

#### 配置文件

可以在根目录下添加 `.rconf.js` 文件

```
module.exports = {
  tempConfigPath: "config.js",
  templateDirPath: "temp",
  tempStaticDir: "static",
  env: "SF",
  opts: {},
  targetStaticDir: "public",
  targetConfigPath: "public/js/config.js",
  configFomatter: function (config) {
    return `window.globalConfigs = ${JSON.stringify(config, null, " ")}`;
  },
};
```

也可以在 `package.json` 中添加属性 `rconf`。

```
// package.json
{
    ...,
    rconf:{
        tempConfigPath: "config.js",
        templateDirPath: "temp",
        tempStaticDir: "static",
        env: "SF",
        opts: {},
        targetStaticDir: "public",
        targetConfigPath: "public/js/config.js",
        configFomatter: function (config) {
            return `window.globalConfigs1 = ${JSON.stringify(config, null, " ")}`;
        },
    },
    ...,

}
```

也可以直接在命令行中添加属性
`rfe run --tempStaticDir static --tempConfigPath config.js --templateDirPath temp`

属性优先级
`命令行 > package.json > .rconf.js`
